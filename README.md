## Descripción
Este proyecto es para fabricar un indicador de marchas para moto. Esta basado en un arduino que controla toda la circuitería y dos sensores hall y un imán para detectar los cambios de marcha.

## Descarga del repositorio
### Descarga directa:
  [indicador-de-marcha-para-moto-master.zip](https://gitlab.com/duffud1/indicador-de-marcha-para-moto/-/archive/master/indicador-de-marcha-para-moto-master.zip)

### Con git:
  git clone https://gitlab.com/duffud1/indicador-de-marcha-para-moto.git

## Requisitos
Arduino IDE 1.8.9 o superior

## Mejoras futuras
* Diseñar dos placas PCB:
- Una con componentes SMD
- Una con componentes "normales"
