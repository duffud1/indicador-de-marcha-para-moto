/*
 * IndicadorMarchaMoto.ino
 * 
 * Copyright 2019 Gabriel Figueira Iglesias
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
 
 // Version: 0.3

#define INPUT_PIN_NEUT    A0      // Neutro
#define INPUT_PIN_HALL1   A1      // Sensor 1 - Subir marcha
#define INPUT_PIN_HALL2   A2      // Sensor 2 - Bajar marcha
#define INPUT_PIN_BRIGHT  A3      // Ajuste del brillo de la tira de LED

#define OUTPUT_PIN_LED_1  3       // D1 - Verde 1
#define OUTPUT_PIN_LED_2  4       // D2 - Verde 2
#define OUTPUT_PIN_LED_3  5       // D3 - Amarillo 1
#define OUTPUT_PIN_LED_4  6       // D4 - Amarillo 2
#define OUTPUT_PIN_LED_5  7       // D5 - Azul
#define OUTPUT_PIN_LED_5  8       // D6 - Azul
#define OUTPUT_PIN_BACKLIGHT  9   // PWM para el brillo de los LED 

#define SENSOR_1  0
#define SENSOR_2  1

int marcha = 0, marcha_old = 1;

//Estado de los sensores
bool sensor[2] = { false, false};
bool sensor_old[2] = { false, false};


void setup(){
  //Configura las entradas y salidas
  //Input pins
  pinMode(INPUT_PIN_HALL1, INPUT_PULLUP);
  pinMode(INPUT_PIN_HALL2, INPUT_PULLUP);
  pinMode(INPUT_PIN_NEUT, INPUT_PULLUP);
  pinMode(INPUT_PIN_BRIGHT, INPUT);
  //Output pins
  //LEDs
  pinMode(OUTPUT_PIN_LED_1, OUTPUT);
  pinMode(OUTPUT_PIN_LED_2, OUTPUT);
  pinMode(OUTPUT_PIN_LED_3, OUTPUT);
  pinMode(OUTPUT_PIN_LED_4, OUTPUT);
  pinMode(OUTPUT_PIN_LED_5, OUTPUT);
  pinMode(OUTPUT_PIN_LED_6, OUTPUT);
  //PWM Bright
  pinMode(OUTPUT_PIN_BACKLIGHT, OUTPUT);
  //Led integrado
  pinMode(13, OUTPUT);

  //Comprobacion inicial de los LED
  digitalWrite(13, HIGH);
  encendido();
  digitalWrite(13, LOW);
}


void loop(){
  //Ajusta el brillo de los LED con la resistencia variable RV1
  analogWrite(OUTPUT_PIN_BACKLIGHT, analogRead(INPUT_PIN_BRIGHT)/4);
  
  //Comprueba si se sube o se baja marcha
  verMarcha();
  
  //Actualiza el display (solo si se cambia de marcha)
  if(marcha != marcha_old){
    actualizarLeds(marcha);
    marcha_old = marcha;
  }
}


void verMarcha(){
  //Neutro
  if(digitalRead(INPUT_PIN_NEUT) == LOW){
    marcha = 0;
    return;
  }
  
  if(checkSensor(SENSOR_1))
    sumaMarcha();
  else if(checkSensor(SENSOR_2))
    restaMarcha();
}

//Actua como un disparador
bool checkSensor(int opc){
  estadoSensor(opc);

  if(sensor[opc] != sensor_old[opc]){
    sensor_old[opc] = sensor[opc];
    return true;
  }
  
  return false;
}

//Actualiza el estado del sensor
void estadoSensor(int opc){
  switch(opc){
    case SENSOR_1:
      if(digitalRead(INPUT_PIN_HALL1) == LOW)
        sensor[SENSOR_1] = true;
      else{
        sensor[SENSOR_1] = false;
        sensor_old[SENSOR_1] = false;
      }
      return;
    
    case SENSOR_2:
      if(digitalRead(INPUT_PIN_HALL2) == LOW)
        sensor[SENSOR_2] = true;
      else{
        sensor[SENSOR_2] = false;
        sensor_old[SENSOR_2] = false;
      }
    return;
  }
}

void sumaMarcha(){
    if(marcha == 0)   //En caso de pasar de neutra a segunda directamente
      marcha = 2;
    else
      marcha++;
    
    if(marcha > 6) marcha = 6;
}

void restaMarcha(){
  marcha--;
  if(marcha < 1) marcha = 1;
}

//Enciende y apaga los LED
void actualizarLeds(int num){
  //Primero apaga los LED
  for(int pin=OUTPUT_PIN_LED_1; pin<=OUTPUT_PIN_LED_5; pin++)
    digitalWrite(pin, LOW);

  if(num == 0) digitalWrite(OUTPUT_PIN_LED_3, HIGH);
  
  switch(num){
    case 6: digitalWrite(OUTPUT_PIN_LED_6, HIGH);
    case 5: digitalWrite(OUTPUT_PIN_LED_5, HIGH);
    case 4: digitalWrite(OUTPUT_PIN_LED_4, HIGH);
    case 3: digitalWrite(OUTPUT_PIN_LED_3, HIGH);
    case 2: digitalWrite(OUTPUT_PIN_LED_2, HIGH);
    case 1: digitalWrite(OUTPUT_PIN_LED_1, HIGH);
    default: return;
  }
}

//Sirve para comprobar el correcto funcionamiento del display al encender
void encendido(){
  int a=0;
  
  analogWrite(OUTPUT_PIN_BACKLIGHT, 255);
  
  for(a=0;a<=6;a++){
    actualizarLeds(a);
    delay(200);
  }

  for(a=5;a>=0;a--){
    actualizarLeds(a);
    delay(200);
  }
  
  analogWrite(OUTPUT_PIN_BACKLIGHT, analogRead(INPUT_PIN_BRIGHT)/4);
  actualizarLeds(0);
}

